<?php

namespace bdb\input;
use bdb\zipcode\ZipcodeInput;
use yii\helpers\Html;
use yii\widgets\MaskedInput;


/**
 * This is just an example.
 */
class ZipCode extends ZipcodeInput
{
 
 	public $mask;
    public $clientOptions;

    public function run()
    {
    	Html::addCssClass($this->options, 'form-control');
     	/*
        if ($this->hasModel()) {
            $input = Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            $input = Html::textInput($this->name, $this->value, $this->options);
        }
      
        */
        $input = MaskedInput::widget([
		    'model' => $this->model,
		    'attribute' => $this->attribute,
		    'mask' => $this->mask,
		    'options' => $this->options,
		    'value' => $this->value,
            'clientOptions' => $this->clientOptions
 		]);

        $this->renderSearch($input, $this->id);
        $this->renderModal();
        $this->registerJs();   
    } 


}
