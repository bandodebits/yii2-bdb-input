<?php

namespace bdb\input;

use kartik\helpers\Html;
//use kartik\widgets\Select2;

use yii\web\AssetBundle;

class Assets extends AssetBundle
{
    public $sourcePath = '@vendor/bandodebits/yii2-bdb-input/assets/multiselect';
    public $css = ['ui.multiselect.css'];
    public $js = ['jquery-ui-1.10.3.custom.min.js', 'ui.multiselect.js'];
} 

/**
 * This is just an example.
 */
class MultiSelect extends \kartik\base\InputWidget
{
    
    public $sortable=false;
    public $searchable=true;
    public $doubleClickable=false;

    public $spinner=false; 
    public $text=false;
    public $select=false;

    public $id='multiselect';

    public $model = false;
    
    public $pivot = null;
    protected $manyMany = null;


    public function __construct(/*$config, $model*/)
    {
        
        
        
 
        
        
        //$this->model
        /*
         
        $select2 = new exSelect2($model, '', array(), array());
        $select2->registerClientScript();

        $this->sortable         = (isset($config['sortable'])        ? $config['sortable'] : false); 
        $this->searchable       = (isset($config['searchable'])      ? $config['searchable'] : false); 
        $this->spinner          = (isset($config['spinner'])         ? $config['spinner'] : false);
        $this->doubleClickable  = (isset($config['doubleClickable']) ? $config['doubleClickable'] : false); 
        $this->text             = (isset($config['text'])            ? $config['text'] : false); 
        $this->select           = (isset($config['select'])          ? $config['select'] : false); 
        $this->id               = (isset($config['id'])              ? $config['id'] : false); 

        if(isset($config['pivotClass']) && isset($config['pivot']))
        {
            $this->model = $this->getModel($model, $config['manyMany'], $config['pivotClass'], $config['pivot'], $config['tId'], $config['modelId']);
            $this->pivotClass = $config['pivotClass'];
        }

        $this->init();
         
         */
    }
    
    function getManyMany($FKs)
    {
        $modelName = $this->model->getTableSchema()->name;
        
        array_walk($FKs, function (&$item, $key)
        {
          $item = array_shift($item);  
        });
        
        $manyMany = $FKs[0] == $modelName ? $FKs[1] : $FKs[0];
        
        return $manyMany;
    }
    
    function  getValues ()
    {   
        $model =  $this->model->{$this->pivot};
        
        $modelName = $this->model->getTableSchema()->name;
        
        $find = $this->model->getRelation($this->pivot);
        $find = new $find->modelClass;
        $_ENV['FKs'] = $find->getTableSchema()->foreignKeys;
        
                
        array_walk($_ENV['FKs'], function (&$item, $key)
        {
            $_temp = array_keys($item);
          $item = array_pop($_temp);  
        });
        
        $_ENV['values'] = [];
        
        array_walk($model, function (&$item, $key)
        { 
            $_ENV['values'][$item->attributes[$_ENV['FKs'][1]]] = $item->attributes;
        });
        
        return $_ENV['values'];
    }
    
    function html()
    {
        $modelName = strtolower($this->model->getTableSchema()->name);
        $manyName = strtolower($this->manyMany->getTableSchema()->name);
        $data = \yii\helpers\ArrayHelper::map($this->manyMany->find()->all(),'id','nome');
        
        print "<div class=\"row\">
            <div id=\"multiSelect_multiselect\" class=\"form-group control-group col-sm-12 \">
            ".Html::activeDropDownList(
                $this->model,//$modelName.'['.$manyName.'][]',
                $this->attribute,
                $data, [
                'multiple' => 'multiple',
                'class' => 'multiselect form-control',
                //'name'  => "$modelName.'['.$manyName][]",
                'multiselect' => 'multiselect',
                'style' => 'display: none;',
                'id' => 'multiselect'
                ]
            )."
            </div>
        </div>";
    }
    
    /**
     *
     * @return void
     */
    function run()
    {    
        $find = $this->model->getRelation($this->attribute);
        $this->manyMany = new $find->modelClass;
        
        
        //$this->pivot = $find->via->from[0].'s'; 
     
   
        
        //gera o html
        $this->html();
        //inclui o js e css
        Assets::register($this->getView());
        $this->registerScripts();
        
        parent::init();
    }

    /* public function getModel ($model, $manyMany, $pc, $pivot, $tId, $modelId)
    {   
       
        $pivotClass = new $pc;
        $x = new $manyMany;
        $primaryKey = $x->tableSchema->primaryKey;
        $array = array(); 

        foreach($model->{$pivot} as $t) {   

            $array[$t->{$primaryKey}] = $t->attributes;   
            $array[$t->{$primaryKey}]['pivot'] =  $pivotClass->findByPk(array(@$tId => @$t->{$primaryKey}, @$modelId => $model->{$model->tableSchema->primaryKey}))->attributes; 
        }

        return $array;
         
         
    }
*/

    /**
     * Registers the JS and CSS Files
     *
     * @return void
     */
    protected function registerScripts()
    {
        
        $params = [];
        
        $params[] = "id:'multiSelect_".$this->id."'";
        $params[] = "sortable:".($this->sortable ? "true" : "false");
        $params[] = "searchable:".($this->searchable ? "true" : "false");
        $params[] = "doubleClickable:".($this->doubleClickable ? "true" : "false");
        $params[] = "model:".($this->model ? json_encode($this->getValues()): "false"); // revisar 
        
        $params[] = "spinner:".($this->spinner ? ($this->model ? "'".$this->spinner."'" : "true") : "false");
        $params[] = "text:".($this->text ? ($this->model ? "'".$this->text."'" : "true") : "false");
        

        if ($this->select) {
            $find = $this->model->getRelation($this->pivot);
            $find = new $find->modelClass;
            $data = $find->getTableSchema()->columns[$this->select];
            $data->enumValues;
            
            $params[] = "select:".json_encode([$this->select => $data->enumValues]);
        } else {
            $params[] = "select:false";
        }

        $parameters = '{' .implode(',', $params). '}';
        
        $js = <<<JS
        $("#$this->id").multiselect($parameters);
JS;
        $this->getView()->registerJs($js);
        
    }
}
