<?php

namespace bdb\input;

use yii;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;


use kartik\helpers\Html;
use kartik\widgets\Select2;



class Enum extends \kartik\base\InputWidget
{
    public $options;
    public $type;

    public function run()
    {
        $this->options = ['placeholder' => Yii::t('app', 'ENUM_DROPDOWN')];
        return $this->getValues();
    }
 


	/**
     * Initializes the addon for text inputs
     */
    public function getValues()
    {
        $value = $this->model->getTableSchema()->columns[$this->attribute];
 
        if (is_array($value->enumValues) && count($value->enumValues) > 0) {
            $dropDownOptions = [];
            foreach ($value->enumValues as $enumValue) {
                $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
            }

            if($this->type == 'RADIO')
                echo Html::activeRadioList($this->model, $this->attribute, $dropDownOptions);  
            else if($this->type == 'DROPDOWN')
            echo Select2::widget([
                'model' => $this->model,
                'attribute' => $this->attribute,
                'options' => $this->options,
                'value' => Yii::t('app', $this->value),
                'data' => $dropDownOptions,
             ]);
        } 
    }

    static function getData($model, $attribute)
    {
        $value = $model->getTableSchema()->columns[$attribute];
 
        if (is_array($value->enumValues) && count($value->enumValues) > 0) {
            $dropDownOptions = [];
            foreach ($value->enumValues as $enumValue) {
                $dropDownOptions[$enumValue] = Yii::t('app', Inflector::humanize($enumValue)) ;
            }

            return $dropDownOptions;
        }
    }

    

    
}