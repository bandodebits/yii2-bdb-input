<?php

namespace bdb\input;

//use yii\widgets\MaskedInput;
use kartik\form\ActiveField;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\MaskedInput;
 

/**
 * This is just an example.
 */
class Masked extends MaskedInput
{
 	public $addon = [];
 	public $template = "{input}";


   	public function run()
    {
    	$this->initAddon();

        $this->template = str_replace('{input}', MaskedInput::widget([
            'model' => $this->model,
            'attribute' => $this->attribute,
            'mask' => $this->mask,
            'options' => $this->options,
            'value' => $this->value,
            'clientOptions' => $this->clientOptions
        ]), $this->template);

        print $this->template;

    } 

    /**
     * Initializes the addon for text inputs
     */
    public function initAddon()
    {
        if (!empty($this->addon)) {
            $addon = $this->addon;
            $prepend = static::getAddonContent(ArrayHelper::getValue($addon, 'prepend', ''));
            $append = static::getAddonContent(ArrayHelper::getValue($addon, 'append', ''));
            $addonText = $prepend . '{input}' . $append;
            $group = ArrayHelper::getValue($addon, 'groupOptions', []);
            Html::addCssClass($group, 'input-group');
            $contentBefore = ArrayHelper::getValue($addon, 'contentBefore', '');
            $contentAfter = ArrayHelper::getValue($addon, 'contentAfter', '');
            $addonText = Html::tag('div', $contentBefore . $addonText . $contentAfter, $group);
            $this->template = str_replace('{input}', $addonText, $this->template);
        }
    }

    /**
     * Parses and returns addon content
     *
     * @param string /array $addon the addon parameter
     *
     * @return string
     */
    public static function getAddonContent($addon)
    {
        if (is_array($addon)) {
            $content = ArrayHelper::getValue($addon, 'content', '');
            $options = ArrayHelper::getValue($addon, 'options', []);
            if (ArrayHelper::getValue($addon, 'asButton', false) == true) {
                Html::addCssClass($options, 'input-group-btn');
                return Html::tag('div', $content, $options);
            } else {
                Html::addCssClass($options, 'input-group-addon');
                return Html::tag('span', $content, $options);
            }
        }
        return $addon;
    }

}
