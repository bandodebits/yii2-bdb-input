<?php

namespace bdb\input;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

use kartik\color\InputWidget;


class ColorInputAsset extends \kartik\base\AssetBundle
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets/colorpallet');
        $this->setupAssets('css', ['css/spectrum']);
        $this->setupAssets('js', ['js/spectrum']);
        parent::init();
    }
}


/**
 * This is just an example.
 */
class Colorpallet extends \kartik\color\ColorInput
{
	 
    /**
     * Registers the needed assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        $value = $this->hasModel() ? Html::getAttributeValue($this->model, $this->attribute) : $this->value;
        $this->html5Options['value'] = $value;
        ColorInputAsset::register($view);
        if ($this->useNative) {
            parent::registerAssets();
            return;
        }
        \kartik\base\Html5InputAsset::register($view);
        $caption = 'jQuery("#' . $this->options['id'] . '")';
        $input = 'jQuery("#' . $this->html5Options['id'] . '")';
        $this->pluginOptions['change'] = new JsExpression("function(color){{$caption}.val(color.toString());}");
        $this->registerPlugin('spectrum', $input);
        $js = <<< JS
{$input}.spectrum('set','{$value}');
{$caption}.on('change', function(){
    {$input}.spectrum('set',{$caption}.val());
});
JS;
        $view->registerJs($js);
    }

    
}
