/*
 * jQuery UI Multiselect
 *
 * Authors:
 *  Michael Aufreiter (quasipartikel.at)
 *  Yanick Rochon (yanick.rochon[at]gmail[dot]com)
 *
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 * http://www.quasipartikel.at/multiselect/
 *
 *
 * Depends:
 *	ui.core.js
 *	ui.sortable.js
 *
 * Optional:
 * localization (http://plugins.jquery.com/project/localisation)
 * scrollTo (http://plugins.jquery.com/project/ScrollTo)
 *
 * Todo:
 *  Make batch actions faster
 *  Implement dynamic insertion through remote calls
 */


(function($) {

    $.widget("ui.multiselect", {
        options: {
            id: false,
            sortable: true,
            searchable: true,
            spinner: false,
            text: false,
            select: false,
            model: null,
            doubleClickable: true,
            animated: 'fast',
            show: 'slideDown',
            hide: 'slideUp',
            dividerLocation: 0.6,
            availableFirst: false,
            nodeComparator: function(node1, node2) {
                var text1 = node1.text(),
                    text2 = node2.text();
                return text1 == text2 ? 0 : (text1 < text2 ? -1 : 1);
            }
        },
        _create: function() {
             this.element.hide();
            this.id = this.element.attr("id");
            this.container = $('<div class="ui-multiselect ui-helper-clearfix ui-widget"></div>').insertAfter(this.element);
            this.count = 0; // number of currently selected options

            //mult = '<div class="asdaaaaaaa">';
            //c_mult = '</div>';

            ////
            //</div></div>




            this.selectedMult2 = $('<div  class="box col-sm-6"><header class="panel-heading"> <div class="input-group text-sm"> <span class="input-group-btn"> <i class="fa fa-list-alt"></i>  </span> <input type="text" placeholder=" procurar..." class="search empty ui-widget-content ui-corner-all input-sm form-control" />  <span class="input-group-btn"> <i class="fa fa-plus add-all"></i> </span> </div> </header></div>')[this.options.availableFirst ? 'prependTo' : 'appendTo'](this.container);
            this.selectedBox2 = $('<div class="box-content"></div>').appendTo(this.selectedMult2);


            this.selectedMult1 = $('<div class="box col-sm-6"><header class="panel-heading"> <div class="input-group text-sm"> <span class="input-group-btn"> <i class="fa fa-list-alt"></i>  </span> <h6></span> <span class="count"> itens selecionados </span></h6><span class="input-group-btn"> <i class="fa fa-minus remove-all"></i> </span> </div> </header></div>').appendTo(this.container);
            this.selectedBox1 = $('<div class="box-content"></div>').appendTo(this.selectedMult1);


            this.selectedContainer = $('<div class="selected"></div>').appendTo(this.selectedBox1);
            this.availableContainer = $('<div class="available"></div>').appendTo(this.selectedBox2);



            //this.selectedActions = $('<div class="actions ui-widget-header ui-helper-clearfix"><span class="count">0 '+$.ui.multiselect.locale.itemsCount+'</span><a href="#" class="remove-all">'+$.ui.multiselect.locale.removeAll+'</a></div>').appendTo(this.selectedContainer);
            //this.availableActions = $('<div class="actions ui-widget-header ui-helper-clearfix"><input type="text" class="search empty ui-widget-content ui-corner-all"/><a href="#" class="add-all">'+$.ui.multiselect.locale.addAll+'</a></div>').appendTo(this.availableContainer);
            this.selectedList = $('<ul class="selected connected-list"><li class="ui-helper-hidden-accessible"></li></ul>').bind('selectstart', function() {
                return false;
            }).appendTo(this.selectedContainer);
            this.availableList = $('<ul class="available connected-list"><li class="ui-helper-hidden-accessible"></li></ul>').bind('selectstart', function() {
                return false;
            }).appendTo(this.availableContainer);

            var that = this;

            /*// set dimensions
		this.container.width(this.element.width()+1);
		this.selectedContainer.width(Math.floor(this.element.width()*this.options.dividerLocation));
		this.availableContainer.width(Math.floor(this.element.width()*(1-this.options.dividerLocation)));

		// fix list height to match <option> depending on their individual header's heights
		this.selectedList.height(Math.max(this.element.height()-this.selectedActions.height(),1));
		this.availableList.height(Math.max(this.element.height()-this.availableActions.height(),1));*/

            if (!this.options.animated) {
                this.options.show = 'show';
                this.options.hide = 'hide';
            }
            // init lists
            this._populateLists(this.element.find('option'));


            // make selection sortable
            if (this.options.sortable) {
                this.selectedList.sortable({
                    placeholder: 'ui-state-highlight',
                    axis: 'y',
                    update: function(event, ui) {
                        // apply the new sort order to the original selectbox
                        that.selectedList.find('li').each(function() {
                            if ($(this).data('optionLink'))
                                $(this).data('optionLink').remove().appendTo(that.element);
                        });
                    },
                    receive: function(event, ui) {
                        ui.item.data('optionLink').attr('selected', true);
                        // increment count
                        that.count += 1;
                        that._updateCount();
                        // workaround, because there's no way to reference 
                        // the new element, see http://dev.jqueryui.com/ticket/4303
                        that.selectedList.children('.ui-draggable').each(function() {
                            $(this).removeClass('ui-draggable');
                            $(this).data('optionLink', ui.item.data('optionLink'));
                            $(this).data('idx', ui.item.data('idx'));
                            that._applyItemState($(this), true);
                        });

                        // workaround according to http://dev.jqueryui.com/ticket/4088
                        setTimeout(function() {
                            ui.item.remove();
                        }, 1);
                    }
                });
            }



            var json = {};
            var _this = this;
            var selectKey = null;
            var selectOptions = null;

            if (!this.options.spinner) this.container.find(".spinner_").remove();

            if (!this.options.text) this.container.find(".text_").remove();

            if (!this.options.select) this.container.find(".select_").remove();
            else {
                $.each(this.options.select, function(key, options) {
                    selectKey = key;
                    selectOptions = options;
                });
            }
      
            $.each(this.element[0], function(key, value) {

                if (typeof $(value).attr('selected') != "undefined") {
 
                    if (_this.options.text) {
                        json.text = _this.options.model[value.value][_this.options.text];
                       _this.container.find("li[title='" + value.text + "'] .text").html(json.text);
                    }
                    if (_this.options.spinner) {
                        json.spinner = _this.options.model[value.value][_this.options.spinner];
                        _this.container.find("li[title='" + value.text + "'] .spinner").val(json.spinner);
                    }
                    if (_this.options.select) {
                        json.select = _this.options.model[value.value][selectKey];
                        _this.container.find("li[title='" + value.text + "'] .select").val(json.select);
                    }
                    
          
                   
                } else {
                    if (_this.options.text) json.text = '';
                    if (_this.options.spinner) json.spinner = 1;
                    if (_this.options.select) json.select = selectOptions[0];
                }

                json.id = value.value;
                value.value = JSON.stringify(json);
             });


            // set up livesearch
            if (this.options.searchable) {
                this._registerSearchEvents(this.selectedMult2.find('input.search'));
            } else {
                $('.search').hide();
            }

            // batch actions
            this.container.find(".remove-all").click(function() {
                that._populateLists(that.element.find('option').removeAttr('selected'));
                return false;
            });

            this.container.find(".add-all").click(function() {
                var options = that.element.find('option').not(":selected");
                if (that.availableList.children('li:hidden').length > 1) {
                    that.availableList.children('li').each(function(i) {
                        if ($(this).is(":visible")) $(options[i - 1]).attr('selected', 'selected');
                    });
                } else {
                    options.attr('selected', 'selected');
                }
                that._populateLists(that.element.find('option'));
                return false;
            });
            //console.log(this.options.model);
            if (this.options.model !== false) {
                this.spinner();
                this.text();
                this.select();
            }
            if (this.options.select !== false) {
                // jQuery('select', '#' + this.options.id).select2({
                //     'placeholder': 'Selecione...'
                // });
            }

        },
        destroy: function() {
            this.element.show();
            this.container.remove();

            $.Widget.prototype.destroy.apply(this, arguments);
        },
        _populateLists: function(options) {
            this.selectedList.children('.ui-element').remove();
            this.availableList.children('.ui-element').remove();
            this.count = 0;

            var that = this;
            var items = $(options.map(function(i) {
                var item = that._getOptionNode(this).appendTo(this.selected ? that.selectedList : that.availableList).show();

                if (this.selected) that.count += 1;
                that._applyItemState(item, this.selected);
                item.data('idx', i);
                return item[0];
            }));

            // update count
            this._updateCount();
            that._filter.apply(this.selectedMult2.find('input.search'), [that.availableList]);
        },
        _updateCount: function() {
            this.element.trigger('change');
            this.selectedMult1.find('span.count').text(this.count + " " + $.ui.multiselect.locale.itemsCount);
        },
        _getOptionNode: function(option) {
            option = $(option);
            selectHtml = '';
            
            selectOptions = false;
            $.each(this.options.select, function(key, options) {
                selectOptions = options;
            });

            if (selectOptions) {
                $.each(selectOptions, function(key, value) {
                    selectHtml += "<option value='" + value + "'>" + value + "</option>";
                });
            }
 
            var html_spinner = !this.options.spinner ? '' : '<div class="spinner_"><i class="fa fa-plus-square"></i><input class="spinner input-sm form-control" type="text" value="1" /><i class="fa fa-minus-square"></i></div>';
            var html_select = !this.options.select ? '' : '<div class="select_"><select class="select">' + selectHtml + '</select></div>';
            var html_text = !this.options.text ? '' : '<div class="text_"><textarea class="text"></textarea></div>';
                        
            var node = $('<li class="ui-state-default ui-element" title="'+option.text()+'"><span class=""/>' + option.text() + ' <div class="top_">'+html_spinner+html_select+'</div> '+html_text+' <a href="#" class="action"><span class="ui-corner-all fa"/></a></li>').hide();
            node.data('optionLink', option);
            return node;
        },
        // clones an item with associated data
        // didn't find a smarter away around this
        _cloneWithData: function(clonee) {
            var clone = clonee.clone(false, false);
            clone.data('optionLink', clonee.data('optionLink'));
            clone.data('idx', clonee.data('idx'));
            return clone;
        },
        _setSelected: function(item, selected) {
            item.data('optionLink').attr('selected', selected);

            if (selected) {
                var selectedItem = this._cloneWithData(item);
                item[this.options.hide](this.options.animated, function() {
                    $(this).remove();
                });
                selectedItem.appendTo(this.selectedList).hide()[this.options.show](this.options.animated);

                this._applyItemState(selectedItem, true);
                return selectedItem;
            } else {

                // look for successor based on initial option index
                var items = this.availableList.find('li'),
                    comparator = this.options.nodeComparator;
                var succ = null,
                    i = item.data('idx'),
                    direction = comparator(item, $(items[i]));

                // TODO: test needed for dynamic list populating
                if (direction) {
                    while (i >= 0 && i < items.length) {
                        direction > 0 ? i++ : i--;
                        if (direction != comparator(item, $(items[i]))) {
                            // going up, go back one item down, otherwise leave as is
                            succ = items[direction > 0 ? i : i + 1];
                            break;
                        }
                    }
                } else {
                    succ = items[i];
                }

                var availableItem = this._cloneWithData(item);
                succ ? availableItem.insertBefore($(succ)) : availableItem.appendTo(this.availableList);
                item[this.options.hide](this.options.animated, function() {
                    $(this).remove();
                });
                availableItem.hide()[this.options.show](this.options.animated);

                this._applyItemState(availableItem, false);
                return availableItem;
            }
        },
        _applyItemState: function(item, selected) {
            if (selected) {
                if (this.options.sortable)
                    item.children('span').addClass('ui-icon-arrowthick-2-n-s').removeClass('ui-helper-hidden').addClass('ui-icon');
                else
                    item.children('span').removeClass('ui-icon-arrowthick-2-n-s').addClass('ui-helper-hidden').removeClass('ui-icon');

                item.find('.spinner_').fadeIn(0);
                item.find('.text_').fadeIn(0);
                item.find('.select_').fadeIn(0);
                item.find('a.action span').addClass('fa-minus').removeClass('fa-plus');
                this._registerRemoveEvents(item.find('a.action'));

            } else {

                item.find('.spinner_').fadeOut(0);
                item.find('.text_').fadeOut(0);
                item.find('.select_').fadeOut(0);
                item.children('span').removeClass('ui-icon-arrowthick-2-n-s').addClass('ui-helper-hidden').removeClass('ui-icon');
                item.find('a.action span').addClass('fa-plus').removeClass('fa-minus');
                this._registerAddEvents(item.find('a.action'));
            }

            this._registerDoubleClickEvents(item);
            this._registerHoverEvents(item);
        },
        // taken from John Resig's liveUpdate script
        _filter: function(list) {
            var input = $(this);
            var rows = list.children('li'),
                cache = rows.map(function() {

                    return $(this).text().toLowerCase();
                });

            var term = $.trim(input.val().toLowerCase()),
                scores = [];

            if (!term) {
                rows.show();
            } else {
                rows.hide();

                cache.each(function(i) {
                    if (this.indexOf(term) > -1) {
                        scores.push(i);
                    }
                });

                $.each(scores, function() {
                    $(rows[this]).show();
                });
            }
        },
        _registerDoubleClickEvents: function(elements) {

            if (!this.options.doubleClickable) return;
            elements.dblclick(function(ev) {
                if ($(ev.target).closest('.action').length === 0) {
                    // This may be triggered with rapid clicks on actions as well. In that
                    // case don't trigger an additional click.
                    elements.find('a.action').click();
                }
            });
        },
        _registerHoverEvents: function(elements) {
            elements.removeClass('ui-state-hover');
            elements.mouseover(function() {
                $(this).addClass('ui-state-hover');
            });
            elements.mouseout(function() {
                $(this).removeClass('ui-state-hover');
            });
        },
        _registerAddEvents: function(elements) {
            var that = this;
            elements.click(function() {
                var item = that._setSelected($(this).parent(), true);
                that.count += 1;
                that._updateCount();
                return false;
            });

            // make draggable
            if (this.options.sortable) {
                elements.each(function() {
                    $(this).parent().draggable({
                        connectToSortable: that.selectedList,
                        helper: function() {
                            var selectedItem = that._cloneWithData($(this)).width($(this).width() - 50);
                            selectedItem.width($(this).width());
                            return selectedItem;
                        },
                        appendTo: that.container,
                        containment: that.container,
                        revert: 'invalid'
                    });
                });
            }
        },
        _registerRemoveEvents: function(elements) {
            var that = this;
            elements.click(function() {
                that._setSelected($(this).parent(), false);
                that.count -= 1;
                that._updateCount();
                return false;
            });
        },
        _registerSearchEvents: function(input) {
            var that = this;

            input.focus(function() {
                $(this).addClass('ui-state-active');
            })
                .blur(function() {
                    $(this).removeClass('ui-state-active');
                })
                .keypress(function(e) {
                    if (e.keyCode == 13)
                        return false;
                })
                .keyup(function() {
                    that._filter.apply(this, [that.availableList]);
                });
        },
        spinner: function() {
            
             //console.log($(this.element)); //$("#list option[value='2']").text()
            //console.log($('input.spinner'));
            var multiselect = $(this.element).find('option');

            function insert(newVal, _this) {
                var item = _this.parents('li').attr('title');
                $.each(multiselect, function(key, value) {
                    if (item == $(value).text()) {
                        var op = jQuery.parseJSON($(multiselect[key]).val());
                        newVal = (newVal === '' ? 1 : newVal);
                        op.spinner = newVal;
                        multiselect[key].value = JSON.stringify(op);
                        return false;
                    }
                });
            }
            $('#'+this.options.id).on('keyup', 'input.spinner', '#' + this.options.id, function(event) {
                 var newVal = this.value.replace(/[^0-9]/g, function(str) {
                    return '';
                });
                $(this).val(newVal);
                insert(newVal, $(this));
            });

            $('#'+this.options.id).on("click", '.spinner_ .fa-minus-square', '#' + this.options.id, function() {
                var input = $(this).parent(this).find(".spinner");
                var spinner = parseInt(input.val());
                spinner--;
                valLimit = spinner > 1 ? spinner : 1;
                input.val(valLimit);
                insert(valLimit, $(this));
            });

            $('#'+this.options.id).on("click", '.spinner_ .fa-plus-square', '#' + this.options.id, function() {
                var input = $(this).parent(this).find(".spinner");
                var spinner = parseInt(input.val());
                spinner++;
                input.val(spinner);
                insert(spinner, $(this));
            });
        },
        text: function() {
            var multiselect = $(this.element).find('option');

            function insert(newVal, _this) {
                var item = _this.parents('li').attr('title');
                $.each(multiselect, function(key, value) {
                    if (item == $(value).text()) {
                        var op = jQuery.parseJSON($(multiselect[key]).val());
                        newVal = (newVal === '' ? 1 : newVal);
                        op.text = newVal;
                        multiselect[key].value = JSON.stringify(op);
                        return false;
                    }
                });
            }

            $('#'+this.options.id).on('keyup', '.text_ textarea.text', function(event) {
                 var newVal = this.value;
                $(this).html(newVal);
                insert(newVal, $(this));
            });
        },
        select: function() {
            
             var multiselect = $(this.element).find('option');

            function insert(newVal, _this) {
                var item = _this.parents('li').attr('title');
                $.each(multiselect, function(key, value) {
                    if (item == $(value).text()) {
                        var op = jQuery.parseJSON($(multiselect[key]).val());
                        newVal = (newVal === '' ? 1 : newVal);
                        op.select = newVal;
                        multiselect[key].value = JSON.stringify(op);
                        return false;
                    }
                });
            }
             $('#'+this.options.id).on('change', '.select_ select.select', function(event) {
                var newVal = this.value;
                //console.log(newVal);
                $(this).attr("selected", "selected");
                insert(newVal, $(this));
            });

        }
    });

    $.extend($.ui.multiselect, {
        locale: {
            addAll: 'Adicionar Todos',
            removeAll: 'Remover Todos',
            itemsCount: 'Itens Selecionados'
        }
    });

    //



})(jQuery);